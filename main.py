import os

from flask import Flask, Response, render_template
from app import app

import loremipsum
import random

class Provider(object):
    @classmethod
    def line(cls):
        count = random.randrange(3,6)
        return ' '.join(loremipsum.get_sentences(count))
    @classmethod
    def par(cls):
        return loremipsum.get_paragraph()
    @classmethod
    def paragraph(cls): # alias
        return cls.par()
    @classmethod
    def post(cls):
        count = random.randrange(3,6)
        return '\n\n'.join(loremipsum.get_paragraphs(count))

@app.route("/")
def main():
    type = 'line'
    text = Provider.line()
    return render_template("main.html", type=type, text=text)

@app.route("/<string:type>")
def provide(type):
    text = getattr(Provider, type, Provider.line)()
    return Response(text, mimetype="text/plain")

if __name__ == "__main__":
    port = 6014
    port = int(os.getenv('PORT', port))
    app.run(port=port)
