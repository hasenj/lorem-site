import os
from flask import Flask

app_name = os.path.basename(os.path.dirname(os.path.abspath(__file__)))
app = Flask(app_name)
if os.getenv('FLASK_TESTING', False):
    app.testing = True
if os.getenv('FLASK_DEBUG', False):
    app.debug = True

# read secret key from env
app.secret_key = os.getenv('FLASK_SECRET', '').decode('string-escape')
