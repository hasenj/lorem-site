

type = ko.observable('line')
url = ko.computed(function() {
    return location.href + type();
});

// from: http://stackoverflow.com/a/6190426/35364
selectElement = function(element) {
    getSelection().removeAllRanges();
    var range = document.createRange();
    range.selectNode(element);
    getSelection().addRange(range);
}

select = function(query) {
    selectElement(document.querySelector(query))
}

TabVM = function(slug, text) {
    var self = this;

    self.is_active = ko.computed(function() {
        return type() == slug;
    })

    self.text = ko.observable(text);

    self.activate = function() {
        type(slug);
        refresh_text();
    }
}

text = ko.observable('');

html = ko.computed(function() {
    return text().replace(/\n/g, "<br>")
});

refresh_text = function() {
    $.get("/" + type(), function(response) {
        text(response);
    })
}

text.subscribe(function() {
    select('#text');
});

tabs = [
    new TabVM("line", "Line"),
    new TabVM("par", "Paragraph"),
    new TabVM("post", "Post/Article")
    ]

app_loaded = ko.observable(false);

// initialize the App via KO
initAppKO = function() {
    ko.applyBindings();
    app_loaded(true);
}

$(initAppKO);
